package kz.attractor.lab56.controller;

import kz.attractor.lab56.annotations.ApiPageable;
import kz.attractor.lab56.dto.TaskDto;
import kz.attractor.lab56.service.TaskService;
import kz.attractor.lab56.service.UserService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
    @RequestMapping("/Task")
    public class TaskController {
        public final TaskService ts;
        public final UserService us;

        public TaskController(TaskService ts, UserService us) {
            this.ts = ts;
            this.us = us;
        }

        @ApiPageable
        @GetMapping
        public Slice<TaskDto> findTasks(@ApiIgnore Pageable pageable) {
            return ts.findTasks(pageable);
        }

        @PostMapping(path = "/newTask")
        public TaskDto newTask(@RequestBody TaskDto taskDto) {
            return ts.newTask(taskDto);
        }

        @GetMapping("/changeType/{id}")
        public TaskDto changeType(@PathVariable("id") String id) {
            return ts.changeType(id);
        }


    }

