package kz.attractor.lab56.controller;

import kz.attractor.lab56.annotations.ApiPageable;
import kz.attractor.lab56.dto.UserDto;
import kz.attractor.lab56.repository.TaskRepository;
import kz.attractor.lab56.repository.UserRepository;
import kz.attractor.lab56.service.TaskService;
import kz.attractor.lab56.service.UserService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/User")
public class UserController {
public final TaskService ts;
public final UserService us;

    public UserController(TaskService ts, UserService us) {
        this.ts = ts;
        this.us = us;
    }

    @ApiPageable
    @GetMapping
    public Slice<UserDto> findPersons(@ApiIgnore Pageable pageable) {
        return us.findUsers(pageable);
    }


    @PostMapping(path = "/reg")
    public UserDto reg(@RequestBody UserDto personDTO) {
        return us.reg(personDTO);
    }
}
