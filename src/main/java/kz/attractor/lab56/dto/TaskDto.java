package kz.attractor.lab56.dto;
import kz.attractor.lab56.model.*;
import lombok.*;

import java.time.LocalDate;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class TaskDto {
    public static TaskDto from(Task task) {
        return builder()
                .id(task.getId())
                .header(task.getHeader())
                .describe(task.getDescribe())
                .type(task.getType())
                .plan(task.getPlan())
                .user(task.getUser())
                .build();
    }

    private String id;
    private String header;
    private String describe;
    private User user;
    private LocalDate plan;
    private String type;

}
