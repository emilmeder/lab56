package kz.attractor.lab56.dto;
import kz.attractor.lab56.model.*;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class UserDto {

    public static UserDto from(User user) {
        return builder()
                .id(user.getId())
                .name(user.getName())
                .mail(user.getMail())
                .password(user.getPassword())
                .build();
    }

    private String id;
    private String name;
    private String mail;
    private String password;

}