package kz.attractor.lab56.model;


import kz.attractor.lab56.util.Generator;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

@Data
@Document
@Builder
public class Task {
    @Id
    private String id;
    private String header;
    private String describe;
    private User user;
    private LocalDate plan;
    private String type;


    public static Task makeNew(User user) {
        return builder()
                .header(Generator.makeName())
                .describe(Generator.makeDescription())
                .user(user)
                .type(newType())
                .plan(newDate())
                .build();
    }

    private static LocalDate newDate(){
        Random random = new Random();
        return LocalDate.of((2021 + random.nextInt(4)),(1 + random.nextInt(12)),(1 + random.nextInt(31)));
    }


    private static String newType(){
        Random random = new Random();
        int i = random.nextInt(3)+1;
        String type = "";
        if(i==1){
            type = "New";
        }

        else if(i==2){
            type =  "Processing";
        }

        else{
            type = "Finished";
        }
        return type;
    }
}
