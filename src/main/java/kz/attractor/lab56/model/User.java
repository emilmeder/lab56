package kz.attractor.lab56.model;

import kz.attractor.lab56.util.Generator;
import kz.attractor.lab56.util.SecurityConfig;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

@Document
@Data
@Builder
public class User implements UserDetails{
    @Id
    private String id;
    private String name;
    private String mail;
    private String password;


    public static User makeNew() {
        return builder()
                .name(Generator.makeName())
                .password("qqw")
                .mail(Generator.makeEmail())
                .build();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("FULL"));
    }

    @Override
    public String getUsername() {
        return getMail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password) {
        this.password = SecurityConfig.encoder().encode(password);
    }

}