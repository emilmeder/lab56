package kz.attractor.lab56.repository;

import kz.attractor.lab56.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User, String> {
    Optional<User> findUserByMail(String mail);

    User findUserByName(String name);
//    public Optional<User> findByUsername(String name);
//
//    User findUserByName(String name);
//
//    User findUserById(String id);
//
//    User findUserByMail(String mail);

//    public Optional<User> findByMail(String mail);

//    boolean existUserByMail(String mail);
}
