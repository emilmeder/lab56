package kz.attractor.lab56.service;

import kz.attractor.lab56.repository.*;
import kz.attractor.lab56.model.*;
import kz.attractor.lab56.dto.*;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;

@Service
public class TaskService {
    public final UserRepository ur;
    public final TaskRepository tr;

    public TaskService(UserRepository ur, TaskRepository tr) {
        this.ur = ur;
        this.tr = tr;
    }


    public Slice<TaskDto> findTasks(Pageable pageable) {
        var slice = tr.findAll(pageable);
        return slice.map(TaskDto::from);
    }

    public TaskDto newTask(TaskDto taskDto){
        Task task = Task.builder()
        .header(taskDto.getHeader())
        .describe(taskDto.getDescribe())
        .plan(taskDto.getPlan())
        .user(taskDto.getUser())
        .type(taskDto.getType())
        .build();
        tr.save(task);
        return TaskDto.from(task);
    }

    public TaskDto changeType(String id){
        String type = "";
        var task = tr.findById(id);

        if(task.get().getType().equals("New")){
            type =  "Processing";
        }

        else if(task.get().getType().equals("Processing")){
            type =  "Finished";
        }

        else{
            type = "Finished";
        }

        tr.delete(task.get());
        task.get().setType(type);
        tr.save(task.get());
        return TaskDto.from(task.get());
    }

}