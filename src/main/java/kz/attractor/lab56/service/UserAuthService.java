package kz.attractor.lab56.service;

import kz.attractor.lab56.model.*;
import kz.attractor.lab56.repository.*;
import kz.attractor.lab56.model.User;
import kz.attractor.lab56.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserAuthService implements UserDetailsService {
    private final UserRepository ur;

    @Override
    public User loadUserByUsername(String userMail) throws UsernameNotFoundException {
        Optional<User> user = ur.findUserByMail(userMail);
//        if (user.isPresent())
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User does not exit");
        }
        return user.get();
    }
}

