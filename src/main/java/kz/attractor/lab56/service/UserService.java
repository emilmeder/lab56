package kz.attractor.lab56.service;

import kz.attractor.lab56.dto.*;
import kz.attractor.lab56.model.*;
import kz.attractor.lab56.repository.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    public final UserRepository ur;
    public final TaskRepository tr;

    public UserService(UserRepository ur, TaskRepository tr) {
        this.ur = ur;
        this.tr = tr;
    }


    public Slice<UserDto> findUsers(Pageable pageable) {
        var slice = ur.findAll(pageable);
        return slice.map(UserDto::from);
    }

    public UserDto reg(UserDto userDto) {
        var user = User.builder()
        .id(userDto.getId())
        .name(userDto.getName())
        .mail(userDto.getMail())
        .password(userDto.getPassword())
        .build();
        ur.save(user);
        return UserDto.from(user);
    }
}