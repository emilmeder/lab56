package kz.attractor.lab56.util;
import kz.attractor.lab56.model.*;
import kz.attractor.lab56.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Configuration
public class DatabasePreloader {
        public final UserRepository ur;
        public final TaskRepository tr;
        private static final Random random = new Random();

        public DatabasePreloader(UserRepository ur, TaskRepository tr) {
            this.ur = ur;
            this.tr = tr;
        }

        @Bean
        CommandLineRunner generateGibberish(UserRepository ur, TaskRepository tr) {
        return (args) -> {
            ur.deleteAll();
            var test = User.makeNew();
            test.setMail("qqw@gmail.com");
            test.setPassword("qqw");
            ur.save(test);

//                                                       the password is "qqw"


            List<User> users = Stream.generate(User::makeNew).limit(5).collect(toList());
            ur.saveAll(users);
            ur.findAll().forEach(e -> System.out.println(e));
            tr.deleteAll();
            List<Task> tasks = Stream.generate(() -> Task.makeNew((users.get(random.nextInt(users.size()))))).limit(10).collect(toList());
            tr.saveAll(tasks);
            tr.findAll().forEach(e -> System.out.println(e));

            };
        }
    }
